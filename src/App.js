import "bootstrap/dist/css/bootstrap.min.css"
import { Container } from "reactstrap";
import Body from "./Components/body";

function App() {
  return (
    <Container style={{backgroundColor: "#b2bec3", padding: "20px", marginTop: "50px"}}>
      <Body/>
    </Container>
  );
}

export default App;
